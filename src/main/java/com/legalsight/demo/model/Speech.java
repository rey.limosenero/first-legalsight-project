package com.legalsight.demo.model;


import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "speech")
public class Speech {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "speech")
    private String speech;

    @Column(name = "author")
    private String author;

    @Column(name = "local_date")
    private String localDate;

    public Speech() {
    }

    public Speech(long id, String speech, String author, String localDate) {
        this.id = id;
        this.speech = speech;
        this.author = author;
        this.localDate = localDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSpeech() {
        return speech;
    }

    public void setSpeech(String speech) {
        this.speech = speech;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    @Override
    public String toString() {
        return "Speech{" +
                "id=" + id +
                ", speech='" + speech + '\'' +
                ", author='" + author + '\'' +
                ", localDate=" + localDate +
                '}';
    }
}
