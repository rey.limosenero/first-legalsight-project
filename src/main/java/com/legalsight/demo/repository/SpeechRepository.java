package com.legalsight.demo.repository;

import com.legalsight.demo.model.Speech;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpeechRepository extends JpaRepository <Speech,Long> {
}
